# ac-computadora-ii

## MAPAS CONCEPTUALES

[[_TOC_]]

### Computadoras Electrónicas

```plantuml
@startmindmap

!theme sketchy-outline

caption Mapa Conceptual 1
center footer Angel Jesús Zorrilla Cuevas - ISB

*[#50fa7b] Computadoras Electrónicas
     ** Computadoras Electromécanicas
          *** Harvard Mark I\n (IBM - 1944)
               **** Características 
                    *****_ 765000 componentes.
                    *****_ 80 km de cable.
                    *****_ Eje de 15 metros.
                    *****_ Motor de 5 caballos de fuerza.
                    *****_ Tenía aproximadamente 3500 relés.
               **** Operaciones
                    *****_ 3 sumas o restas por segundo.
                    *****_ Las multiplicaciones en 6 segundos.
                    *****_ Divisiones en 15 segundos.
                    *****_ Operaciones complejas en más de un minuto.
               **** Usos
                    ***** Sumulaciones y cálculos para el proyecto Manhattan.
          *** The Bombe (Alan Turing - 1940)
               **** Fue diseñado para desifrar el codigo enigma nazi.
          *** Componente principal
               **** Relé
                    ***** Es un interruptor mecánico controlado eléctricamente \nque estaba compuesto por una bobina, un brazo de \nhierro móvil y dos contactos.
                    
                    *****_ Funcionamiento
                         ****** Cuando una corriente eléctrica atraviesa la bobina se genera \nun campo magnético, el campo magnético atráe al brazo métalico, \nesto une los dos contactos cerrando el circuito.

     ** Computadoras Electrónicas 
          *** Colossus Mark I \n(Tommy Flowers - 1943)
               ****_ Fue instalada en el Reino Unido y ayudaba \na decodificar las comunicaciones nazis.
               ****_ Tenía 1600 tuvos de vacío
               ****_ Fue conocida como la primera computadora \nelectrónica programable.
               ****_ La programación se realizaba con cientos \nde cables en un tablero.
          *** ENIAC (John Mauchly & \nJ. Presper Exkert - 1946)
               ****_ Calculadora Integradora Numérica Electrónica
               ****_ Primera computadora electrónica programable de \npropósito general
               ****_ Podía realizar 5000 sumas y restas de 10 dígitos \npor segundo
          *** Componente principal         
               **** Válvula termoiónica \n(1904 - John Fleming)
                    ***** Estaba formado por un filamento y dos \neléctrodos que están situados dentro de un \nbulbo de cristal sellado.
                         ****** Electrodo de Control
                              ******* Estaba situado entre el ánodo y \nel cátodo del diseño de Fleming.
                              *******_ Funcionamiento
                                   ******** Al aplicar una carga \npositiva o negativa, el \neléctrodo de control, permitía \nel flujo o detenía la corriente \nrespectivamente.
                    ***** Funcionamiento
                         ****** Cuando el filamento se enciende y se calienta, \npermite el flujo de electrones del ánodo hacia el \ncátodo.
                              ****** A este proceso se le llama emisión \ntermoiónica y permite el flujo de una corriente \nen una sola dirección. A esto se le conoce \nahora como diodo
               **** Transistores (1947)
                    ***** Cumple al misma función que la de un relé o de un \ntubo de vació, ya que funciona como interruptor eléctrico.
                    ***** Características
                         ******_ Están hechos de silicio.
                         ******_ Son más solidos y resistentes.
                         ******_ Podía cambiar de estado unas 10,000 veces por segundo.
                    ***** Construcción
                         ******_ Compuesto por 3 capas de silicio en forma de \nsandwich.
                         ******_ Cada capa representa un conector y estos \nconectores son llamados Colector, Emisor y Base.
                    ***** Funcionamiento
                         ******_ Cuando hay un flujo de corriente entre la base \ny el emisor, éste permite que exista un flujo de \ncorriente entre el emisor y el colector, cumpliendo \ncon el comportamiento de un interruptor eléctrico.


@endmindmap

```

### Arquitectura Von Neumann y Arquitectura Harvard

```plantuml
@startmindmap

!theme sketchy-outline

caption Mapa Conceptual 2
center footer Angel Jesús Zorrilla Cuevas - ISB

*[#50fa7b] Arquitectura de Computadoras
     ** Ley de Moore
          *** Ley de Moore
               **** Establece que la velocidad del procesador o el \npoder de cada procesamiento total de las \ncomputadoras se duplica cada año.
               **** Electrónica
                    *****_ El número de transistores totales por chip se duplica cada año.
                    *****_ El costo del chip permanece sin cambios.
                    *****_ Cada 18 meses se duplica la potencia de cálculo sin modificar el costo.
               **** Performance
                    *****_ Se incrementa la velocidad del procesador.
                    *****_ Se incrementa la capacidad de memoria.
                    *****_ La velocidad de la memoria corre siempre \npor detrás de la velocidad del procesador.
     ** Funcionamiento de \nuna computadora
          ***_ Antes
               **** Programaciòn mediante Hardware
                    ***** Cuando se quiere realiar otra tarea se debe \ncambiar el hardware.
          ***_ Ahora
               **** Programaciòn mediante Software
                    ***** En cada paso se realiza alguna operaciòn \nsobre los datos, a travès de un intèrprete de \ninstrucciones.
     
     ** Arquitectura Von Neumann
          ***_ Contiene
               **** Este describe una arquitectura de \ndiseño de una computadora digital \nelectrònico con 3 partes:
                    ***** CPU 
                         ****** Unidad Central de Procesamiento, la cual contiene \nuna unidad de control, una unidad aritmética lógica y registros.
                    ***** Memoria
                         ****** La cual puede almacenar tanto instrucciones como datos.
                    ***** Sistema de Entrada/Salida.
          ***_ Modelo
               **** Es una arquitectura basada en la descrita \nen 1945 por el matemático y \nfísico John Von Neumann y otros.
                    ***** Los datos y programas se almacenan en una misma memoria.
                    ***** Los contenidos de esta memoria se acceden identificando \nsu posición sin importar su tipo.
                    ***** Ejecución en secuencia.
                    ***** Representación binaria.
               ****_ Interconexión 
                    ***** Todos los componentes se comunican a través de \nun sistema de buses.
                         ******_ se dividen en
                              ******* Bus de Datos.
                              ******* Bus de Direcciones.
                              ******* Bus de Control.
                         ******_ ¿Qué es un bus?
                              ******* Es un dispositivo en común \nentre dos o más dispositivos, si \ndos dispositivos transmiten al \nmismo tiempo señales, estas pueden \ndistorsionarse y consecuentemente  \nperder información.
                         
                              ******* Existen varios tipos de buses \nque realizan la tarea de \ninterconexión entre las distintas \npartes del computador, al bus que \ncomunica al procesador, memoria y E/S \nse le denomina bus del sistema.

          ***_ Surge
               **** El concepto de programa almacenado, por el cuál \nse les conoce a las computadoras de este tipo.
               **** Cuello de botella de Neumann
                    ***** La cantidad de datos que pasa entre estos \ndos elementos difiere mucho en tiempo con las \ncondiciones de velocidades de ellos, por lo \ncual la CPU puede permanecer ociosa.
          
          ***_ Funcionamiento
               **** La función de una computadora es la \nejecución de programas. Los programas se    ➤ \nencuentran localizados en la memoria.
                    ***** La CPU ejecuta dichas instrucciones a   ➤\ntravés de un ciclo de instrucciones.
                         ****** Las instrucciones consisten de \nsecuencias de 1 y 0, llamadas código máquina    ➤\ny no son legibles para las personas.
                              ******* Por ello se emplean lenguajes como el \nensamblador (Bajo nivel) o lenguajes de \nprogramación como Java(Alto nivel).

               ****_ Ciclo de ejecución 
                    ***** PASO 1: UC obtiene la próxima \ninstrucción de memoria y dejando la \niformación en el registro IR.
                         ******_ Fetch de Instrucción (FI)
                    ***** PASO 2: Se incrementa el PC.
                    ***** PASO 3: La instrucción es decodificada \na un lenguaje que entiende la ALU.
                         ******_ Decode de Instrucción (DI)
                    ***** PASO 4: Obiene de memoria los operando \nrequeridos por la instrucción.
                         ******_ Calcular Operandos (CO)
                         ******_ Fetch de Operandos (FO)
                    ***** PASO 5: La ALU ejecuta y deja los \nresultados en registros o en memoria.
                         ******_ Execute Instrucción (EI)
                         ******_ Write Operand (WO)
                    ***** PASO 6: Volver al paso 1.
                         
     ** Arquitectura Harvard \nArquitectura Microcontroladores
          ***_ Contiene
               **** Este describe una arquitectura de diseño \npara un computador digital electrónico con \ndiferentes partes.
                    *****_ Consta de
                         ****** Unidad Central de Procesamiento
                         ****** Memoria Principal 
                         ****** Sistemas de E/S
                         
          ***_ Modelo
               **** Esta arquitectura originalmente se referia a \nlas arquitecturas de computadoras que utilizaban \ndispositivos de almacenamiento físicamente \nseparados para las instrucciones y para los datos \n(En oposición a la arquitectura de Von Neumann).
          ***_ Arquitectura
               **** Memoria Caché
                    ***** Fabricar memorias mucho más rápidas tiene \nun precio muy alto. La solución a esto feu la \ncreación de la memoria caché que es muy \npequeña pero mucho más rápida que la principal.
               **** Procesador
                    ***** Dos unidades funcionales \nprincipales
                         ****** Unidad de Control (UC)
                              *******_ La UC lee instrucciones de la memoria de \ninstrucciones.
                              *******_ Genera las señales de control para obtener \nlos operandos de memoria de datos.
                         ****** Unidad Aritmética Lógica (ALU)
                              *******_ La UC ejecuta las instrucciones mediante la ALU, \nalmacenando el resultado en la memoria de datos.
               **** Memoria de Instrucciones
                    ***** Es la memoria donde se almacenan \nlas instrucciones del programa que debe \nejecutar el microcontrolador.
                    ***** La memoria de instrucciones se \nimplementa utilizando memorias no \nvolátiles:
                         ******_ ROM
                         ******_ PROM
                         ******_ EPROM
                         ******_ EEPROM
               **** Memoria de Datos
                    ***** Se almacenan los datos utilizados \npor los programas.
                    ***** Los datos varían constantemente y, \npor lo tanto utiliza la memoria RAM. \nSobre la cual se pueden realizar \noperaciones de lectura y escritura.
                         ******_ SRAM
                         ******_ EEPROM

               
               

@endmindmap

```

### Basura Electrónica

```plantuml
@startmindmap

!theme sketchy-outline

caption Mapa Conceptual 3
center footer Angel Jesús Zorrilla Cuevas - ISB

*[#50fa7b] Basura Electrónica
     ** ¿Qué es?
          *** Son aquellos dispositivos eléctricos y electrónicos que \nhan llegado al final de su vida útil y son desechados.
          *** A este tipo de desechos se denomina a nivel mundial \ncomo Residuos de Aparatos Eléctricos y Electrónicos (RAEE).
     ** Consecuencias
          *** Contaminan el suelo, agua y aire
          *** Problemas de salud por la quema de estos aparatos
          *** Las baterias pueden contaminar 600 mil litros de agua
          *** Algunas placas base y baterias recargables tienen metales \npesados que pueden generar cáncer en lar personas
     ** Elementos que se pueden extraer
          ***_ Cobre
          ***_ Oro
          ***_ Plata
          ***_ Cobalto
          ***_ Hierro
          ***_ Aluminio
          ***_ Datos/Información
     ** Soluciones
          *** Donar o vender los equipos electrónicos que ya no ocupemos.
          *** Reducir la generación de desechos electrónicos a través de la compra responsable.
          *** Depositar los aparatos electrónicos en su lugar correspondiente
          *** Hay empresar que reciclan estos aparatos.
               **** Centros de Reciclaje
                    *****_ e-end (EE. UU.)
                    *****_ Perú Green Recycling (Perú)
                    *****_ Techemet (México)
                    *****_ REMSA (México)


@endmindmap

```